<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Prostoy
 */
?>

					</div><!-- #content -->

					<footer id="colophon" class="site-footer" role="contentinfo">
						<div class="site-info">
							<a href="<?php echo esc_url( __( 'http://wordpress.org/', 'prostoy' ) ); ?>"><?php printf( __( 'Proudly powered by %s', 'prostoy' ), 'WordPress' ); ?></a>
							<span class="sep"> | </span>
							<?php printf( __( 'Theme: %1$s by %2$s.', 'prostoy' ), 'Prostoy', '<a href="http://underscores.me/" rel="designer">Underscores.me</a>' ); ?>
						</div><!-- .site-info -->
					</footer><!-- #colophon -->
				</div><!-- #page -->
			</div><!-- Main -->
		</div>
	</div>
</div>
<div class="pure-u-xl-1-5 pure-u-md-2-24 pure-u-1"></div>
<?php wp_footer(); ?>

</body>
</html>
