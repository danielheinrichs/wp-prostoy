<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Prostoy
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site pure-g">
	<div class="pure-u-xl-1-5 pure-u-md-2-24 pure-u-1"></div>
	<div class="pure-u-xl-3-5 pure-u-md-20-24 pure-u-1">
		<div class="pure-g" id="content-holder">
			<div class="pure-u-1">
				<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'prostoy' ); ?></a>
				<header id="masthead" class="site-header pure-g" role="banner">
					<div class="pure-u-1 center">
						<div class="site-branding">
							<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
							<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
						</div><!-- .site-branding -->
					</div>
				</header><!-- #masthead -->
				<div class="pure-g" id="navigation">
					<div class="pure-u-1 center">
						<nav id="site-navigation" class="main-navigation" role="navigation">
							<button class="menu-toggle" aria-controls="menu" aria-expanded="false"><?php _e( 'Primary Menu', 'prostoy' ); ?></button>
							<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
						</nav><!-- #site-navigation -->
					</div>
				</div>

				<div id="content" class="site-content">